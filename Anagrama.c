/*
Se llama a la libreria stdio.h para poder imprimir
datos por pantalla.
*/
#include <stdio.h>
 
/*
Firma de la función check_anagram.
*/
int check_anagram(char *a, char *b);

/*
Declaración de variables para los arreglos donde se
almacenarán las palabras ingresadas por teclado, y los
punteros para recorrer.
*/
char a[100], b[100];
char *pa;
char *pb;
  
/*
Definición de la función main, aquí se asigna al arreglo y al puntero
la palabra ingresada por el usuario. La función muestra por pantalla
el resultado de verificar si son o no anagramas.
*/
int main(){
  printf("Inroducir la primera palabra:\n");
  pa=gets(a);		//La función gets nos permite tomar lo ingresado por el usuario y lo pasamos al puntero.
 
  printf("Inroducir la segunda palabra:\n", b);
  pb=gets(b);
	  
  if (check_anagram(a, b) == 1)
    printf("Las palabras ingresadas son Anagramas! :)\n");
  else
    printf("Las palabras ingresadas NO son Anagramas! :(\n");
 
  return 0;
}

/*
Definición de la función check_anagram, la cual recibe como parámetro dos
punteros de tipo char. Se definen dos arreglos de tamaño 26 tipo int para
almacenar la frecuencia de las letras en cada palabra. Al final la función
retorna 0 si los arreglos tipo int son diferentes o 1 si son iguales.
*/
int check_anagram(char *a, char *b){
  int first[26] = {0}, second[26] = {0};
  
  /*
  Recorrido de cada puntero que contiene la palabra hasta el
  final y se suma 1, en la posición de acuerdo al abecedario.
  */
  while (*a!='\0'){
    first[*a-'a']++;
    a++;
  }
    
  while (*b!='\0'){
    second[*b-'a']++;
    b++;
  }  

  /*
  Recorrido de los arreglos tipo int para determinar si son
  o no iguales, y retornar un valor.
  */
  for (int c = 0; c < 26; c++){
    if (first[c] != second[c])
      return 0;
  }
 
  return 1;
}