<<ORGANIZACI�N DE COMPUTADORES>>

<<PROYECTO PARCIAL>>

Integrantes: 

	Carlos Sesme Vera 
	
	Emilio Mor�n Pilligua 

<<PROGRAMA ANAGRAMA>>

El programa verifica si dos cadenas de caracteres o string (en min�scula), que 
son ingresados por el usuario mediante le teclado son o no anagramas, se 
muestra por consola el resultado al usuario.  

Dos palabras son anagramas, cuando contienen las mismas cantidad y frecuencias
de letras. Por lo tanto, se puede tener dos palabras con significados 
diferentes utilizando las mismas letras. 

El programa est� escrito tanto en el lenguaje C y el Assembler. El objetivo 
del proyecto es llevar el c�digo en lenguaje C a lenguaje ensamblador.  