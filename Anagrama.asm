.data
## Se inicializan los arreglos con los cuales se validara si las palabras 
## son o no anagramas
first: .word 0 : 26		
second: .word 0 : 26

## String que se muestran en pantalla		
equalStr: .asciiz "Las palabras ingresadas son Anagrama! :)\n"		
equalStr1: .asciiz "Las palabras ingresadas NO son Anagrama! :(\n"
pedidoP1: .asciiz "Inroducir la primera palabra:\n"
pedidoP2: .asciiz "Introducir la segunda palabra:\n"

## Se inicializan los arreglos que contendran las palabras ingresadas por teclado
entradaP1: .space 100 
entradaP2: .space 100
.text
## funcion main
main:	
	## se imprime: pedidoP1 ("Inroducir la primera palabra:\n")
	li $v0, 4		## Se indica al sistema que se imprimira una cadena (se almacena 4 en $v0)
	la $a0, pedidoP1	## se almacena en $a0 la cadena a imprimir
	syscall			## se llama al sistema
	
	## se ingresa por tecado la primera palabra que se almacenra en entradaP1
	li $v0, 8		## Se indica al sistema que lea una cadena por consola(Ingreso por teclado)
	la $a0, entradaP1	## Se almacena la direccion de la cadena donde se guardara el ingreso
	li $a1, 20		## se especifica la longitud del arreglo
	syscall			## se llama al sistema
	
	## Se imprime: pedidoP2 ("Introducir la segunda palabra:\n")
	li $v0, 4		## Se indica al sistema que se imprimira una cadena (se almacena 4 en $v0)
	la $a0, pedidoP2	## se almacena en $a0 la cadena a imprimir
	syscall			## se llama al sistema
	
	## se ingresa por tecado la segunda palabra que se almacenara en entradaP2
	li $v0, 8		## Se indica al sistema que lea una cadena por consola(Ingreso por teclado)
	la $a0, entradaP2	## Se almacena la direccion de la cadena donde se guardara el ingreso
	li $a1, 20		## se especifica la longitud del arreglo
	syscall			## se llama al sistema
	
	jal check_anagram	## Se llama a la funcion check_anagram la cual se ejecutara primero y continuara 
				## apartir de este punto una vez terminada 
	beq $v0, $zero,printN	## se verifica el contenido de $v0 if($v0 == 0) se llama a printE
	bne $v0, $zero,printE	## if($v0 != 0) se llama a printN

	## se termina el programa cada vez que se llama a este funcion
	End:
		addi $v0, $0, 10	## se indica al sistema que se termina el programa (se almacena 10 en $v0)
		syscall			## se llama al sistema
		
	## se imprime el mensaje contenido en equalStr, en caso de que las palabras sean anagramas
	printE: 
		la $a0, equalStr	## se almacena equalStr en $a0 para ser impreso en pantalla
		li $v0, 4		## se indica al sistema que si imprimira una cadena (se almacena 4 en $v0)
		syscall			## se llama al sistema
		j End			## se llama a la funcion End para terminar el programa
	
	## se imprime el mensaje contenido en equalStr1, en caso de que las palabras no sean anagramas
	printN:
		la $a0, equalStr1	## se almacena equalStr1 en $a0 para ser impreso en pantalla
		li $v0, 4		## se indica al sistema que si imprimira una cadena (se almacena 4 en $v0)
		syscall			## se llama al sistema
		j End			## se llama a la funcion End para terminar el programa

## Esta funcion se encarga de analizar las palabras ingresadas y determinar si son o no anagramas	
check_anagram: 

		la $t0, entradaP1	## Se almacena en $t0 la direccion de entradaP1 que contiene la primera palabra ingresada
	
	## se recorre entradaP1 caracter por caracter y se modifica el arreglo first
	while1:  
		lb $t1, 0($t0) 			## se almacena en $t1 el primer caracter del arreglo
		beq $t1, $zero, fin1		## se verifica que el caracter almacenado en $t1 sea diferente de null o cero (ciclo while)
						## caso contrario se llama a fin1
		sub $t1,$t1,'a' 		## se realiza la diferencia entre los valores para obtener el indice (resta de ascii)
						## i-"a"
		li $t2, 4 			## la operacion mult solo funciona con direcciones por lo cual se carga a $t2 la constante 4
		mult  $t1, $t2 			##se realiza la multiplicacion para determinar el indice correcto del arreglo ya que son enteros
		mflo $s0 			## la operacion mult retorna un valor de 64 bits q se almacena en registros especiales por ello se usa
      	       					## la operacion mflo retorna dicho valor en 32 bits solo los valores menos significativos
      	       					## por lo cual el registro $s0 almacena el indice (se deben usar resgitros "s" para mflo)
		lw $t4, first($s0) 		## se extrae el elemento contenido en el arreglo first, 
      	       			 		##ubicado en la posicion $s0, dentro del registro $t4
		addi $t4,$t4,1  		## al valor que se extrae del arreglo se le suma 1 
		sw $t4, first($s0)  		## se vuelve a almacenar el valor ubicado en $t4 en la misma posicion, es decir se reemplaza el valor
						## first[i-'a']++;
		addi $t0, $t0, 1		## nos movemos una posicion en el arreglo caracteres i++;	       
		j while1  			## se llama nuevamente al ciclo

      	## la funcion fin se encarga de almacenar a $t0 la direccion de entradaP2 que es la segunda palabra
	fin1:     
		la $t0, entradaP2
	
	## la funcion while recorre la segunda palabra y modifica el arreglos second por cada letra		 
	while2:  
		lb $t1, 0($t0) 			## se almacena en $t1 el primer caracter del arreglo
		beq $t1, $zero, fin3 		## se verifica que el caracter almacenado en $t1 sea diferente de null o cero (ciclo while)
						## caso contrario se llama a fin3
		sub $t1,$t1,'a' 		## se realiza la diferencia entre los valores para obtener el indice (resta de ascii)
						## i-"a"
		li $t2, 4 			## la operacion mult solo funciona con direcciones por lo cual se carga a $t2 la constante 4
		mult  $t1, $t2 			##se realiza la multiplicacion para determinar el indice correcto del arreglo ya que son enteros
		mflo $s0 			## la operacion mult retorna un valor de 64 bits q se almacena en registros especiales por ello se usa
      	       					## la operacion mflo retorna dicho valor en 32 bits solo los valores menos significativos
      	       					## por lo cual el registro $s0 almacena el indice (se deben usar resgitros "s" para mflo)
		lw $t4, second($s0)		## se extrae el elemento contenido en el arreglo second, 
      	       			  		## ubicado en la posicion $s0, dentro del registro $t4
		addi $t4,$t4,1  		## al valor que se extrae del arreglo se le suma 1 
		sw $t4, second($s0)  		## se vuelve a almacenar el valor ubicado en $t4 en la misma posicion, es decir se reemplaza el valor
						## first[i-'a']++;
		addi $t0, $t0, 1		## nos movemos una posicion en el arreglo caracteres   	       
		j while2  			## se llama nuevamente al ciclo
      	       
      	## la funcion fin3 almacena en $t1 y $t2 los arreglos modificados first y second respectivamente 
	fin3:
		li $t0,0			## se almacenan constante para usala como contador
		li $v0,0		
		la $t1, first			## se almacena first en $t0
		la $t2, second			## se almacena second en $t1
						## una vez terminada fin3 se llama a loop
	##  loop se encarga de comparar los arreglos first y second en caso de ser diferentes
	## la palabra no es un anagrama y si son iguales las palabras son anagramas
	loop:	bgt $t0,25,exit			## se compara el contador $t0 con 25 for ($t0=0,$t0<25,$t0++)
		addi $t0,$t0,1			## se suma 1 al contador $t0
		lw $t3, 0($t1)			## se extrae en $t3 el primer elemento de first
		lw $t4, 0($t2)			## se extrae en $t4 el primer elemento de second
		add  $t1, $t1, 4		## nos movemos una espacio en el arreglo first
		add  $t2, $t2, 4		## nos movemos un espacio en el arreglo second
		bne $t3,$t4,exit1		## se compara los valores de de $t3 y $t4 if($t3 != $t4) {las palabras no son anagramas}
		j loop				## se llama a la funcion nuevamente
	
	
	## la funcion exit1 se llamara solo si los arreglos son diferentes en alguno de sus elementos lo que 
	## indica que no son anagramas
	exit1:	addi $v0, $zero,0 		## se modifica el valor de $v0 con 0
		jr $ra				## return 0
	
	## el funcion exit se llama solo si los arreglos son iguales lo que indica que las palabras son anagramas
	exit: 	addi $v0,$zero,1		## se modifica el valor de $v0 con 1
		jr $ra 				## return 1

